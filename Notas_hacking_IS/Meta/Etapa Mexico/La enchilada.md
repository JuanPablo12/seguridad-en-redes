# La enchilada

## Solucion
Nos dan un archivo el cual debemos de abrir con la aplicacion de wireshark. Una vez dentro damos clic derecho sobre una de las lineas y le damos en follow TCP Stream. Nos dirigimos asta el 37 y ahi se encuentra el siguiente texto:
```Texto
..... ..#..'.............. .....'.................!Debian GNU/Linux 10

localhost login: enchilada

Password:

Last login: Tue Sep 13 11:59:50 CDT 2022 from 148.226.168.220 on pts/0

Linux localhost 4.19.0-16-amd64 #1 SMP Debian 4.19.181-1 (2021-03-19) x86_64

  

The programs included with the Debian GNU/Linux system are free software;

the exact distribution terms for each program are described in the

individual files in /usr/share/doc/*/copyright.

  

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent

permitted by applicable law.

.]0;enchilada@localhost: ~.enchilada@localhost:~$ pwd

/home/enchilada

.]0;enchilada@localhost: ~.enchilada@localhost:~$ ls

bandera flag.txt

.]0;enchilada@localhost: ~.enchilada@localhost:~$ more flag.txt

ZmxhZ01Ye0VuY2gxbGFkYVIwamF5VmVyZDN9Cg==

.]0;enchilada@localhost: ~.enchilada@localhost:~$ clear

.[H.[2J.[3J.]0;enchilada@localhost: ~.enchilada@localhost:~$ id

uid=1018(enchilada) gid=1018(enchilada) groups=1018(enchilada)

.]0;enchilada@localhost: ~.enchilada@localhost:~$ exit

logout
```

De ese texto sacamos una parte la cual parece estar encriptada en base 64 solo hay que hacer la conversion y listo
```bandera encriptada
ZmxhZ01Ye0VuY2gxbGFkYVIwamF5VmVyZDN9Cg==
```

```Bandera convertida
flagMX{Ench1ladaR0jayVerd3}
```

## Referencias
https://gchq.github.io/CyberChef/#recipe=From_Base64('A-Za-z0-9%2B/%3D',true,false)