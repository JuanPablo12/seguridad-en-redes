# Eden mine

## Solucion
Hay que sacar la frecuencia con la que se repiten los caracteres y apuntar esos numeros, los cuales son los siguientes:
```
102 108 97 103 77 88 123 73 95 49 48 118 51 95 102 114 51 113 117 51 110 99 105 101 115 95 88 68 125
```

Con esto nos damos cuenta que esta cifrado con el sistema Decimal, solo hay que descifrarlo y listo
```
flagMX{I_10v3_fr3qu3ncies_XD}
```

## Referencias
https://www.contadorpalabras.com/
https://gchq.github.io/CyberChef/#recipe=From_Decimal('Space',false)&input=MTAyIDEwOCA5NyAxMDMgNzcgODggMTIzIDczIDk1IDQ5IDQ4IDExOCA1MSA5NSAxMDIgMTE0IDUxIDExMyAxMTcgNTEgMTEwIDk5IDEwNSAxMDEgMTE1IDk1IDg4IDY4IDEyNQ