# Maria's Art Class

## Solucion
```bash
┌──(kali㉿kali)-[~/CTF/MariaArt]
└─$ strings KUKULKAN.jpg | grep FLAG
_I LOVE FLAGS :3 
.MY FAVOURITE WORD IS FLAG 
THIS IS NOT THE FLAG, KEEP LOOKING BRO! 
/ FLAG Un PAso mas  BRO{Vm0weGQxTnRVWGxXYTFwUFZsZG9WRmxVU2xOalJsSlZVMnhPVlUxV2NEQlVWbEpUWVdzeFYxTnNhRmRpVkZaUVZrUktTMUl5VGtsaVJtUk9ZbTFvZVZadE1YcGxSbGw0Vkc1V2FsSnNXazlXYlRWRFYxWmFkR1JIUmxSTlZYQjVWR3hhYjFSc1duTmpSVGxhWWxoU1RGWnNXbUZXVmtaMFVteHdWMkpJUWpaV1ZFa3hWREZhV0ZOclpHcFNWR3hZV1ZSS1VrMUdWWGRYYlVacVZtdHdlbGRyV210VWJGcDFVV3R3VjJGcmJ6QlZla1pYVmpGa2NsWnNTbGRTTTAwMQ==}   x
                                                                          
┌──(kali㉿kali)-[~/CTF/MariaArt]
└─$ 

```

Simplemente le aplicamos base 64 asta que nos de la bandera
```
flagMX{R3TURN_TH3_J4GUARS_EYES}
```

## Referencias
https://gchq.github.io/CyberChef/#recipe=From_Base64('A-Za-z0-9%2B/%3D',true,false)From_Base64('A-Za-z0-9%2B/%3D',true,false)From_Base64('A-Za-z0-9%2B/%3D',true,false)From_Base64('A-Za-z0-9%2B/%3D',true,false)From_Base64('A-Za-z0-9%2B/%3D',true,false)From_Base64('A-Za-z0-9%2B/%3D',true,false)From_Base64('A-Za-z0-9%2B/%3D',true,false)From_Base64('A-Za-z0-9%2B/%3D',true,false)&input=Vm0wd2VHUXhUblJWV0d4WFlURndVRlpzWkc5V1JteFZVMnhPYWxKc1NsWlZNbmhQVmxVeFYyTkVRbFZXYkVwVVdWZHplRll4VG5OaFJtUnBWa1phVVZaclVrdFRNVWw1Vkd0c2FWSnRVazlaYlRGdlpWWmFkRTFZY0d4U2JHdzBWa2MxVjJGc1NuTlhhemxYWWxSV1JGWXhXbUZrUjFKSVVteFNUbFpZUWpWV1IzaGhZakZTYzFkdVRtcFNWR3hoV1d4b1UxUkdXbk5YYlVaWFZtdGFNRlZ0ZUhkV01rcEpVV3BhVjFaRmEzaFdSRVpoVjBaT2NscEhjRk5XUjNoWlYxWlNTMVZyTVVkV1dHUllZbFZhY1ZadGRIZGxiR1J5VjIxMFZXSkdjREZWVjNSM1ZqSkdjbUo2UWxabGExcFlWbXBHYTJOc1duTlRiR1JUVFRBd01RPT0