from pwn import *

file = open('space_encoded.txt','rb')
data = bytearray(file.read())
data = data.replace(b'\t',b'1')
data = data.replace(b'\x20',b'0')
data = data.replace(b'\r\n',b'')
data = data.decode('ascii')
data = unbits(data)

print(data)
