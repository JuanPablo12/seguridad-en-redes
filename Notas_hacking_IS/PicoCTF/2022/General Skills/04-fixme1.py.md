# fixme1.py

## Objetivo
Fix the syntax error in this Python script to print the flag. [Download Python script](https://artifacts.picoctf.net/c/39/fixme1.py)

## Solucion
```bash
┌──(kali㉿kali)-[~/Downloads]
└─$ python3 fixme1.py 
  File "/home/kali/Downloads/fixme1.py", line 20
    print('That is correct! Here\'s your flag: ' + flag)
IndentationError: unexpected indent
                                                                                                    
┌──(kali㉿kali)-[~/Downloads]
└─$ nano fixme1.py 
                                                                                                    
┌──(kali㉿kali)-[~/Downloads]
└─$ nano fixme1.py
                                                                                                    
┌──(kali㉿kali)-[~/Downloads]
└─$ python3 fixme1.py
That is correct! Here is your flag: picoCTF{1nd3nt1ty_cr1515_182342f7}
                                                                                                    
┌──(kali㉿kali)-[~/Downloads]
└─$ 

```
## Notas
Nos pidio arreglar una mala identacion que habia en el documento.

## Referencias