# runme.py

## Objetivo
Run the `runme.py` script to get the flag. Download the script with your browser or with `wget` in the webshell. [Download runme.py Python script](https://artifacts.picoctf.net/c/86/runme.py)

## Solucion
```bash
┌──(kali㉿kali)-[~/Downloads]
└─$ python3 runme.py 
picoCTF{run_s4n1ty_run}
                                                                                
┌──(kali㉿kali)-[~/Downloads]
└─$ 

```
## Notas

## Referencias