# PW Crack 3

## Objetivo
Can you crack the password to get the flag? Download the password checker [here](https://artifacts.picoctf.net/c/23/level3.py) and you'll need the encrypted [flag](https://artifacts.picoctf.net/c/23/level3.flag.txt.enc) and the [hash](https://artifacts.picoctf.net/c/23/level3.hash.bin) in the same directory too. There are 7 potential passwords with 1 being correct. You can find these by examining the password checker script.

## Solucion
```bash
┌──(kali㉿kali)-[~/Downloads]
└─$ python3 level3.py
Please enter correct password for flag: 4b17
Welcome back... your flag, user:
picoCTF{m45h_fl1ng1ng_2b072a90}
                                                                                                    
┌──(kali㉿kali)-[~/Downloads]
└─$ 

```
## Notas
Revisando el archivo level3.py en la parte inferior del archivo viene una lista con los passwords posibles, asi que fui intentando uno por uno.
"6997", "3ac8", "f0ac", "4b17", "ec27", "4e66", "865e"
## Referencias