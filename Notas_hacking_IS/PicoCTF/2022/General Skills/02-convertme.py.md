# convertme.py

## Objetivo
Run the Python script and convert the given number from decimal to binary to get the flag. [Download Python script](https://artifacts.picoctf.net/c/31/convertme.py)

## Solucion
```bash
┌──(kali㉿kali)-[~/Downloads]
└─$ python3 convertme.py 
If 43 is in decimal base, what is it in binary base?
Answer: 101011
That is correct! Here is your flag: picoCTF{4ll_y0ur_b4535_9c3b7d4d}
                                                                                                    
┌──(kali㉿kali)-[~/Downloads]
└─$ 

```
## Notas

## Referencias
https://www.rapidtables.org/convert/number/decimal-to-binary.html
