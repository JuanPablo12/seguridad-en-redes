# Secrets

## Objetivo
We have several pages hidden. Can you find the one with the flag? The website is running [here](http://saturn.picoctf.net:49917/).

## Solucion
```bash
                                                                                                              
┌──(kali㉿kali)-[~]
└─$ ffuf -w /usr/share/seclists/Discovery/Web-Content/raft-medium-words.txt -u http://saturn.picoctf.net:49917/FUZZ

        /'___\  /'___\           /___\       
       /\ \__/ /\ \__/  __  __  /\ \__/       
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\      
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/      
         \ \_\   \ \_\  \ \____/  \ \_\       
          \/_/    \/_/   \/___/    \/_/       

       v1.5.0 Kali Exclusive <3
________________________________________________

 :: Method           : GET
 :: URL              : http://saturn.picoctf.net:49917/FUZZ
 :: Wordlist         : FUZZ: /usr/share/seclists/Discovery/Web-Content/raft-medium-words.txt
 :: Follow redirects : false
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 200,204,301,302,307,401,403,405,500
________________________________________________

.                       [Status: 200, Size: 1023, Words: 201, Lines: 37, Duration: 111ms]
secret                  [Status: 301, Size: 169, Words: 5, Lines: 8, Duration: 111ms]
:: Progress: [63087/63087] :: Job [1/1] :: 353 req/sec :: Duration: [0:03:16] :: Errors: 0 ::
                                                                                                              
┌──(kali㉿kali)-[~]
└─$ ffuf -w /usr/share/seclists/Discovery/Web-Content/raft-medium-words.txt -u http://saturn.picoctf.net:49917/secret/FUZZ

        /'___\  /'___\           /''___\       
       /\ \__/ /\ \__/  __  __  /\ \__/       
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\      
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/      
         \ \_\   \ \_\  \ \____/  \ \_\       
          \/_/    \/_/   \/___/    \/_/       

       v1.5.0 Kali Exclusive <3
________________________________________________

 :: Method           : GET
 :: URL              : http://saturn.picoctf.net:49917/secret/FUZZ
 :: Wordlist         : FUZZ: /usr/share/seclists/Discovery/Web-Content/raft-medium-words.txt
 :: Follow redirects : false
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 200,204,301,302,307,401,403,405,500
________________________________________________

assets                  [Status: 301, Size: 169, Words: 5, Lines: 8, Duration: 110ms]
.                       [Status: 200, Size: 468, Words: 55, Lines: 13, Duration: 107ms]
hidden                  [Status: 301, Size: 169, Words: 5, Lines: 8, Duration: 108ms]
:: Progress: [63087/63087] :: Job [1/1] :: 357 req/sec :: Duration: [0:03:08] :: Errors: 0 ::
                                                                                                              
┌──(kali㉿kali)-[~]
└─$ 

```

Con esto nos damos cuenta que debemos ingresar a la pagina secret, despues a la carpeta hidden y depues de ir a esta nos vamos al codigo fuente de la pagina que nos muestra.

```html
<!DOCTYPE html>
<html>
  <head>
    <title>LOGIN</title>
    <!-- css -->
    <link href="[superhidden/login.css](view-source:http://saturn.picoctf.net:49917/secret/hidden/superhidden/login.css)" rel="stylesheet" />
  </head>
  <body>
    <form>
      <div class="container">
        <form method="" action="[/secret/assets/popup.js](view-source:http://saturn.picoctf.net:49917/secret/assets/popup.js)">
          <div class="row">
            <h2 style="text-align: center">
              Login with Social Media or Manually
            </h2>
            <div class="vl">
              <span class="vl-innertext">or</span>
            </div>

            <div class="col">
              <a href="[#](view-source:http://saturn.picoctf.net:49917/secret/hidden/#)" class="fb btn">
                <i class="fa fa-facebook fa-fw"></i> Login with Facebook
              </a>
              <a href="[#](view-source:http://saturn.picoctf.net:49917/secret/hidden/#)" class="twitter btn">
                <i class="fa fa-twitter fa-fw"></i> Login with Twitter
              </a>
              <a href="[#](view-source:http://saturn.picoctf.net:49917/secret/hidden/#)" class="google btn">
                <i class="fa fa-google fa-fw"></i> Login with Google+
              </a>
            </div>

            <div class="col">
              <div class="hide-md-lg">
                <p>Or sign in manually:</p>
              </div>

              <input
                type="text"
                name="username"
                placeholder="Username"
                required
              />
              <input
                type="password"
                name="password"
                placeholder="Password"
                required
              />
              <input type="hidden" name="db" value="superhidden/xdfgwd.html" />

              <input
                type="submit"
                value="Login"
                onclick="alert('Thank you for the attempt but oops! try harder. better luck next time')"
              />
            </div>
          </div>
        </form>
      </div>

      <div class="bottom-container">
        <div class="row">
          <div class="col">
            <a href="[#](view-source:http://saturn.picoctf.net:49917/secret/hidden/#)" style="color: white" class="btn">Sign up</a>
          </div>
          <div class="col">
            <a href="[#](view-source:http://saturn.picoctf.net:49917/secret/hidden/#)" style="color: white" class="btn">Forgot password?</a>
          </div>
        </div>
      </div>
    </form>
  </body>
</html>
```

Despues de intentar entrar a:
```
http://saturn.picoctf.net:49917/secret/hidden/superhidden/xdfgwd.html
```

nos damos cuenta que no nos lleva a nada, entonces intentamos solo a:
```
http://saturn.picoctf.net:49917/secret/hidden/superhidden/
```
Esto nos va a mandar a otra pagina y ya solo hay que inspeccionar el codigo donde nos dara la bandera.
```html
<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <link rel="stylesheet" href="[mycss.css](view-source:http://saturn.picoctf.net:49917/secret/hidden/superhidden/mycss.css)" />
  </head>

  <body>
    <h1>Finally. You found me. But can you see me</h1>
    <h3 class="flag">picoCTF{succ3ss_@h3n1c@10n_790d2615}</h3>
  </body>
</html>
```
## Referencias


## Notas