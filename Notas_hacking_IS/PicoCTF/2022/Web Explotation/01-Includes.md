# Includes

## Objetivo
Can you get the flag? Go to this [website](http://saturn.picoctf.net:57833/) and see what you can discover.

## Solucion
```CSS
body {
  background-color: lightblue;
}

/*  picoCTF{1nclu51v17y_1of2_  */
```

```js
function greetings()
{
  alert("This code is in a separate file!");
}

//  f7w_2of2_b8f4b022}
```

picoCTF{1nclu51v17y_1of2_f7w_2of2_b8f4b022}
## Referencias


## Notas