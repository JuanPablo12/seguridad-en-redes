# Power cookie

## Objetivo
Can you get the flag? Go to this [website](http://saturn.picoctf.net:61304/) and see what you can discover.

## Solucion
Basado en lo que nos dice el reto hay que poner en la cookie de admin el valor de 1, para que nos de acceso a la parte de admin.
picoCTF{gr4d3_A_c00k13_0d351e23}

## Referencias


## Notas