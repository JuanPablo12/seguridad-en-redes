# Roboto Sans

## Objetivo
The flag is somewhere on this web application not necessarily on the website. Find it. Check [this](http://saturn.picoctf.net:64710/) out.

## Solucion
Sacamos la informacion que hay en el archivo robots.txt:
```
User-agent *
Disallow: /cgi-bin/
Think you have seen your flag or want to keep looking.

ZmxhZzEudHh0;anMvbXlmaW
anMvbXlmaWxlLnR4dA==
svssshjweuiwl;oiho.bsvdaslejg
Disallow: /wp-admin/
```
Despues le aplicamos base64 al valor de anMvbXlmaWxlLnR4dA== y nos dara la direccion del archivo donde esta la bandera:
```
js/myfile.txt
```
Ya solo es cuestion de ponerla en la url como si fuera el archivo robots.txt y nos daran la bandera.

picoCTF{Who_D03sN7_L1k5_90B0T5_032f1c2b}
## Referencias


## Notas