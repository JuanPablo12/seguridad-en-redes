# m00nwalk

## Objetivo
Decode this [message](https://jupiter.challenges.picoctf.org/static/14393e18d98fedbaedbc28896d7ef31a/message.wav) from the moon.

## Solucion
```bash
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/moonwalk]
└─$ sstv -d message.wav -o result.png        
[sstv] Searching for calibration header... Found!    
[sstv] Detected SSTV mode Scottie 1
[sstv] Decoding image...   [##################################################################] 100%
[sstv] Drawing image data...
[sstv] ...Done!
```
![[result.png|600]]
picoCTF{beep_boop_im_in_$pace}
## Notas

## Referencias
