# Shark on wire 1

## Objetivo
There's something in the [building](https://jupiter.challenges.picoctf.org/static/011955b303f293d60c8116e6a4c5c84f/buildings.png). Can you retrieve the flag?

## Solucion
picoCTF{StaT31355_636f6e6e}
Para solucionarlo usamos la herramienta wireshark abrimos el archivo que nos da el reto y vamos a buscar los archivos UCP dando clic derecho en alguno de este tipo. Despues en follow y vamos avanzando de uno en uno asta encontrar la bandera.

## Notas

## Referencias