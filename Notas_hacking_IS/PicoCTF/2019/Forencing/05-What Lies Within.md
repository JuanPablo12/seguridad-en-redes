# What Lies Within

## Objetivo
There's something in the [building](https://jupiter.challenges.picoctf.org/static/011955b303f293d60c8116e6a4c5c84f/buildings.png). Can you retrieve the flag?

## Solucion
```bash
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/What lies within]
└─$ zsteg -a buildings.png | grep pico
b1,rgb,lsb,xy       .. text: "picoCTF{h1d1ng_1n_th3_b1t5}"

```

## Notas
La herramienta zsteg hace muchas todas las pruebas que conoce para sacar la informacion

## Referencias