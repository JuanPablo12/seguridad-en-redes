# waves over lambda

## Objetivo
We made a lot of substitutions to encrypt this. Can you decrypt it? Connect with `nc jupiter.challenges.picoctf.org 13758`

## Solucion
```
-------------------------------------------------------------------------------
zqiyedlm jwew bm sque hgdy - hewfuwizs_bm_z_qnwe_gdpord_rinlheldsu
-------------------------------------------------------------------------------
jdnbiy jdr mqpw lbpw dl ps rbmvqmdg kjwi bi gqirqi, b jdr nbmblwr ljw oeblbmj pumwup, dir pdrw mwdezj dpqiy ljw oqqcm dir pdvm bi ljw gboedes ewyderbiy ledimsgndibd; bl jdr mleuzc pw ljdl mqpw hqewciqkgwryw qh ljw zquiles zqugr jdergs hdbg lq jdnw mqpw bpvqeldizw bi rwdgbiy kblj d iqogwpdi qh ljdl zquiles. b hbir ljdl ljw rbmlebzl jw idpwr bm bi ljw walewpw wdml qh ljw zquiles, tuml qi ljw oqerwem qh ljeww mldlwm, ledimsgndibd, pqgrdnbd dir oucqnbid, bi ljw pbrml qh ljw zdevdljbdi pquildbim; qiw qh ljw kbgrwml dir gwdml ciqki vqelbqim qh wueqvw. b kdm iql dogw lq gbyjl qi dis pdv qe kqec ybnbiy ljw wadzl gqzdgbls qh ljw zdmlgw redzugd, dm ljwew dew iq pdvm qh ljbm zquiles dm swl lq zqpvdew kblj que qki qeridizw muenws pdvm; oul b hquir ljdl obmleblx, ljw vqml lqki idpwr os zquil redzugd, bm d hdbegs kwgg-ciqki vgdzw. b mjdgg wilwe jwew mqpw qh ps iqlwm, dm ljws pds ewhewmj ps pwpqes kjwi b ldgc qnwe ps lednwgm kblj pbid.

```

```
------------------------------------------------------------------------------- CONGRATS HERE IS YOUR FLAG - FREQUENCY_IS_C_OVER_LAMBDA_DNVTFRTAYU 
------------------------------------------------------------------------------- HAVING HAD SOME TIME AT MY DISPOSAL WHEN IN LONDON, I HAD VISITED THE BRITISH MUSEUM, AND MADE SEARCH AMONG THE BOOKS AND MAPS IN THE LIBRARY REGARDING TRANSYLVANIA; IT HAD STRUCK ME THAT SOME FOREKNOWLEDGE OF THE COUNTRY COULD HARDLY FAIL TO HAVE SOME IMPORTANCE IN DEALING WITH A NOBLEMAN OF THAT COUNTRY. I FIND THAT THE DISTRICT HE NAMED IS IN THE EXTREME EAST OF THE COUNTRY, JUST ON THE BORDERS OF THREE STATES, TRANSYLVANIA, MOLDAVIA AND BUKOVINA, IN THE MIDST OF THE CARPATHIAN MOUNTAINS; ONE OF THE WILDEST AND LEAST KNOWN PORTIONS OF EUROPE. I WAS NOT ABLE TO LIGHT ON ANY MAP OR WORK GIVING THE EXACT LOCALITY OF THE CASTLE DRACULA, AS THERE ARE NO MAPS OF THIS COUNTRY AS YET TO COMPARE WITH OUR OWN ORDNANCE SURVEY MAPS; BUT I FOUND THAT BISTRITZ, THE POST TOWN NAMED BY COUNT DRACULA, IS A FAIRLY WELL-KNOWN PLACE. I SHALL ENTER HERE SOME OF MY NOTES, AS THEY MAY REFRESH MY MEMORY WHEN I TALK OVER MY TRAVELS WITH MINA.
```
## Notas

## Referencias