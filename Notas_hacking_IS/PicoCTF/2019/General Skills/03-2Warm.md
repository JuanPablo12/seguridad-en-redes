# 2Warm

## Objetivo
Can you convert the number 42 (base 10) to binary (base 2)?

## Solucion
42
1 2 4 8 16 32
0 1 0 1  0  1

picoCTF{101010}

## Notas

## Referencias
https://www.rapidtables.org/convert/number/decimal-to-binary.html