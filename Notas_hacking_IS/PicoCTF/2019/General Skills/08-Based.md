# Based

## Objetivo
To get truly 1337, you must understand different data encodings, such as hexadecimal or binary. Can you get the flag from this program to prove you are on the way to becoming 1337? Connect with `nc jupiter.challenges.picoctf.org 15130`.

## Solucion
```bash
┌──(kali㉿kali)-[~]
└─$ nc jupiter.challenges.picoctf.org 15130
Let us see how data is stored
colorado
Please give the 01100011 01101111 01101100 01101111 01110010 01100001 01100100 01101111 as a word.
...
you have 45 seconds.....

Input:
colorado
Please give me the  156 165 162 163 145 as a word.
Input:
nurse
Please give me the 636f6d7075746572 as a word.
Input:
computer
You have beaten the challenge
Flag: picoCTF{learning_about_converting_values_02167de8}

```

## Notas
Primero se convierte de binario a texto, despues de octal a texto y por ultimo de hexadecimal a texto.

## Referencias
https://www.rapidtables.com/convert/number/binary-to-ascii.html
http://www.unit-conversion.info/texttools/octal/
https://www.rapidtables.com/convert/number/hex-to-ascii.html