# Lets Warm Up

## Objetivo
If I told you a word started with 0x70 in hexadecimal, what would it start with in ASCII?

## Solución
```bash
┌──(kali㉿kali)-[~]
└─$ python
Python 3.10.7 (main, Sep  8 2022, 14:34:29) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> int(0x70)
112
>>> chr(112)
'p'
>>> 

```

## Notas
pyhon 
int(0x70)
chr(112)
'p'


## Referencias
