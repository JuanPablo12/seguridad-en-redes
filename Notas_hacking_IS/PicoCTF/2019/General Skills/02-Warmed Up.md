# Warmed Up

## Objetivo
What is 0x3D (base 16) in decimal (base 10)?

## Solución
```bash
juanvir-picoctf@webshell:~$ python
Python 3.10.4 (main, Jun 29 2022, 12:14:53) [GCC 11.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> int(0x3D)
61
>>> 
```