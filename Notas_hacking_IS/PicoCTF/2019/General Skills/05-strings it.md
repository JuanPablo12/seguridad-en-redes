# strings it

## Objetivo
Can you find the flag in [file](https://jupiter.challenges.picoctf.org/static/94d00153b0057d37da225ee79a846c62/strings) without running it?

## Solucion
```bash
┌──(kali㉿kali)-[~/Downloads]
└─$ strings strings | grep pico
picoCTF{5tRIng5_1T_d66c7bb7}
                                                                             
┌──(kali㉿kali)-[~/Downloads]
└─$ 

```
## Notas

## Referencias
https://respontodo.com/como-usar-el-comando-strings-en-linux/
https://linux.die.net/man/1/strings