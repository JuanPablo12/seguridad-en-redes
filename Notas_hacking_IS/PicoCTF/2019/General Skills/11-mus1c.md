# mus1c

## Objetivo
I wrote you a [song](https://jupiter.challenges.picoctf.org/static/c0863a3b0170d6dd176be3a595b4b75e/lyrics.txt). Put it in the picoCTF{} flag format.

## Solucion
Para resolverlo nos apoyamos de una pagina para interpretar lo que nos daba el reto lo cual nos sio lo siguiente:
114 114 114 111 99 107 110 114 110 48 49 49 51 114
De ahi sacamos los caracteres de cada numero.
Nos podemos apoyar de python para sacar la cadena de caracteres y no tener que sacarlos de uno por uno.
```bash
──(kali㉿kali)-[~/Downloads]
└─$ nano progam.py                         
                                                                                                     
┌──(kali㉿kali)-[~/Downloads]
└─$ python3 progam.py        
picoCTF{rrrocknrn0113r}
                                                                                                     
┌──(kali㉿kali)-[~/Downloads]
└─$ 

```
## Notas
Este reto esta basado en un lenguaje de programacion llamado Rockstar. 

## Referencias
https://codewithrockstar.com/online
