# First Grep

## Objetivo
Can you find the flag in [file](https://jupiter.challenges.picoctf.org/static/315d3325dc668ab7f1af9194f2de7e7a/file)? This would be really tedious to look through manually, something tells me there is a better way.

## Solucion
```bash
┌──(kali㉿kali)-[~/Downloads]
└─$ cat file | grep pico
picoCTF{grep_is_good_to_find_things_f77e0797}

```
## Notas 

## Referencias
https://www.freecodecamp.org/espanol/news/grep-command-tutorial-how-to-search-for-a-file-in-linux-and-unix-with-recursive-find/