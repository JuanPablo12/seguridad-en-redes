# vault-door-5

## Objetivo
In the last challenge, you mastered octal (base 8), decimal (base 10), and hexadecimal (base 16) numbers, but this vault door uses a different change of base as well as URL encoding! The source code for this vault is here: [VaultDoor5.java](https://jupiter.challenges.picoctf.org/static/d31ce4356bdfd15d33a9af7e35ab4d0a/VaultDoor5.java)

## Solucion
Revisamos el codigo que nos dan para ver que es lo que hace.
```java
import java.net.URLDecoder;

import java.util.*;

  

class VaultDoor5 {

public static void main(String args[]) {

VaultDoor5 vaultDoor = new VaultDoor5();

Scanner scanner = new Scanner(System.in);

System.out.print("Enter vault password: ");

String userInput = scanner.next();

String input = userInput.substring("picoCTF{".length(),userInput.length()-1);

if (vaultDoor.checkPassword(input)) {

System.out.println("Access granted.");

} else {

System.out.println("Access denied!");

}

}

  

// Minion #7781 used base 8 and base 16, but this is base 64, which is

// like... eight times stronger, right? Riiigghtt? Well that's what my twin

// brother Minion #2415 says, anyway.

//

// -Minion #2414

public String base64Encode(byte[] input) {

return Base64.getEncoder().encodeToString(input);

}

  

// URL encoding is meant for web pages, so any double agent spies who steal

// our source code will think this is a web site or something, defintely not

// vault door! Oh wait, should I have not said that in a source code

// comment?

//

// -Minion #2415

public String urlEncode(byte[] input) {

StringBuffer buf = new StringBuffer();

for (int i=0; i<input.length; i++) {

buf.append(String.format("%%%2x", input[i]));

}

return buf.toString();

}

  

public boolean checkPassword(String password) {

String urlEncoded = urlEncode(password.getBytes());

String base64Encoded = base64Encode(urlEncoded.getBytes());

String expected = "JTYzJTMwJTZlJTc2JTMzJTcyJTc0JTMxJTZlJTY3JTVm"

+ "JTY2JTcyJTMwJTZkJTVmJTYyJTYxJTM1JTY1JTVmJTM2"

+ "JTM0JTVmJTY1JTMzJTMxJTM1JTMyJTYyJTY2JTM0";

return base64Encoded.equals(expected);

}

}
```

Nos damos cuenta de que realiza un conversion de base 64 y url encode a un texto el cual regresa esto despues de ambas conversiones:
```
JTYzJTMwJTZlJTc2JTMzJTcyJTc0JTMxJTZlJTY3JTVmJTY2JTcyJTMwJTZkJTVmJTYyJTYxJTM1JTY1JTVmJTM2JTM0JTVmJTY1JTMzJTMxJTM1JTMyJTYyJTY2JTM0
```
Y al hacerlo al contrario obtenemos esta bandera. Se uso cyberchef para realizar la conversion.
```
picoCTF{c0nv3rt1ng_fr0m_ba5e_64_e3152bf4}
```

Tambien se puede hacer en python de la siguiente manera
```python
Python 3.10.7 (main, Sep  8 2022, 14:34:29) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from base64 import b64decode
>>> from urllib.parse import unquote
>>> cad = "JTYzJTMwJTZlJTc2JTMzJTcyJTc0JTMxJTZlJTY3JTVmJTY2JTcyJTMwJTZkJTVmJTYyJTYxJTM1JTY1JTVmJTM2JTM0JTVmJTY1JTMzJTMxJTM1JTMyJTYyJTY2JTM0"
>>> cad = b64decode(cad)
>>> cad
b'%63%30%6e%76%33%72%74%31%6e%67%5f%66%72%30%6d%5f%62%61%35%65%5f%36%34%5f%65%33%31%35%32%62%66%34'
>>> cad = unquote(cad)
>>> cad
'c0nv3rt1ng_fr0m_ba5e_64_e3152bf4'
>>> 
```
## Notas


## Referencias