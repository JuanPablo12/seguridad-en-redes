# B1ll_Gat35

## Objetivo
Can you reverse this [Windows Binary](https://jupiter.challenges.picoctf.org/static/0ef5d0d6d552cd5e0bd60c2adbddaa94/win-exec-1.exe)?

## Solucion
- Ejecutar el binario con wine 
- wine win-exec-1.exe
- ejecutamos el ghidra
- creamos un proyecto vacio
	- importamo sel archivo que queremos decompilar
	- Una vez descompilamos ejecutamos el archivo nuevamente con wine y buscamos unas de las cadenas que aparecen al ejecutarlo.
	- Nos vamos a search program text, todos los campos
	- Cambiar el JNZ por un JNC para eliminar el if que nos hace salir del programa original
	- Lo exportamos
## Referencias


## Notas
instalar wine

sudo apt install wine32:1386