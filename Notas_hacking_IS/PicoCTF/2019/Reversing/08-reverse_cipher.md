# reverse_cipher

## Objetivo
We have recovered a [binary](https://jupiter.challenges.picoctf.org/static/7aa5f383ec616fe9d72c2ffe1fabd0d9/rev) and a [text file](https://jupiter.challenges.picoctf.org/static/7aa5f383ec616fe9d72c2ffe1fabd0d9/rev_this). Can you reverse the flag.

## Solucion
cifrado = open('rev_this','r').read()

print(cifrado)
flag = ''

for i in range(8,len(cifrado)-1):
	if i & i == 0:
		flag += chr(ord(cifrado[i])-5)
	else:
		flag += chr(ord(cifrado[i])+2)
print(flag)
## Notas

reto 9 
sudo apt install gdb

disassemble main
set disassemnbly-flavor intel
disassemble main

## Referencias