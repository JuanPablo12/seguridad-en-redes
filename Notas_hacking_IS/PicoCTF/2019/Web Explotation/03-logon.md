# Logon

## Objetivo
The factory is hiding things from all of its users. Can you login as Joe and find what they've been looking at?

## Solucion
picoCTF{th3_c0nsp1r4cy_l1v3s_6edb3f5f}

## Notas
Se descargo el plugin cookie-editor para poder ver el password generado con las cookies

## Referencias
https://developer.mozilla.org/es/docs/Web/HTTP
https://developer.mozilla.org/es/docs/Web/HTTP/Cookies
https://es.wikipedia.org/wiki/Cookie_(inform%C3%A1tica)
https://addons.mozilla.org/en-US/firefox/addon/cookie-editor/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search
https://developer.mozilla.org/es/docs/Web/HTTP/Methods