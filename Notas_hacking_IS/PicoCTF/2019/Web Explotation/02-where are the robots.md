# where are the robots

## Objetivo
Can you find the robots?

## Solucion
picoCTF{ca1cu1at1ng_Mach1n3s_8028f}

## Notas
Si al final de las paginas agregamos /robots.txt nos mostrara las paginas a las que no quieren que entremos

## Referencias
https://developers.google.com/search/docs/advanced/robots/intro?hl=es