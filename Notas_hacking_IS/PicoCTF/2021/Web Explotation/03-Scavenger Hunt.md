# Scavenger Hunt

## Objetivo
There is some interesting information hidden around this site [http://mercury.picoctf.net:55079/](http://mercury.picoctf.net:55079/). Can you find it?

## Solucion
```html
<!doctype html>

<html>
<head>
<title>Scavenger Hunt</title>
<link href="[https://fonts.googleapis.com/css?family=Open+Sans|Roboto](https://fonts.googleapis.com/css?family=Open+Sans|Roboto)" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="[mycss.css](http://mercury.picoctf.net:55079/mycss.css)">
<script type="application/javascript" src="[myjs.js](http://mercury.picoctf.net:55079/myjs.js)"></script>
</head>
<body>
<div class="container">
<header>
<h1>Just some boring HTML</h1>
</header>
<button class="tablink" onclick="openTab('tabintro', this, '#222')" id="defaultOpen">How</button>
<button class="tablink" onclick="openTab('tababout', this, '#222')">What</button>
<div id="tabintro" class="tabcontent">
<h3>How</h3>
<p>How do you like my website?</p>
</div>
<div id="tababout" class="tabcontent">
<h3>What</h3>
<p>I used these to make this site: <br/>
HTML <br/>
CSS <br/>
JS (JavaScript)
</p>
<!-- Here's the first part of the flag: picoCTF{t -->
</div>
</div>
</body>

</html>
```
```css
div.container {
    width: 100%;
}
header {
    background-color: black;
    padding: 1em;
    color: white;
    clear: left;
    text-align: center;
}
body {
    font-family: Roboto;
}
h1 {
    color: white;
}
p {
    font-family: "Open Sans";
}
.tablink {
    background-color: #555;
    color: white;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    font-size: 17px;
    width: 50%;
}
.tablink:hover {
    background-color: #777;
}
.tabcontent {
    color: #111;
    display: none;
    padding: 50px;
    text-align: center;
}
#tabintro { background-color: #ccc; }
#tababout { background-color: #ccc; }
/* CSS makes the page look nice, and yes, it also has part of the flag. Here's part 2: h4ts_4_l0 */
```
Para la parte 3 solo habia que poner en el url la pagina robots.txt
Para la parte 4 hay que accesar al archivo .htaccess
Y para la parte 5 nos decia que le gustaba usar mac y las mac crean un archivo llamado .DS_Store al cual debemos de ingresar igual que en los anteriores.
picoCTF{th4ts_4_l0t_0f_pl4c3s_2_lO0k_74cceb07}
## Notas

## Referencias