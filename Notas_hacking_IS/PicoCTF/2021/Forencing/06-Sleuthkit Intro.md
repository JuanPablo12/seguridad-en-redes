# Sleuthkit Intro

## Objetivo
Download the disk image and use `mmls` on it to find the size of the Linux partition. Connect to the remote checker service to check your answer and get the flag. Note: if you are using the webshell, download and extract the disk image into `/tmp` not your home directory.

-   [Download disk image](https://artifacts.picoctf.net/c/114/disk.img.gz)
-   Access checker program: `nc saturn.picoctf.net 52279`

## Solucion
```bash
                                                                                                     
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/wiresharktwoo]
└─$ cd ..           
                                                                                                     
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos]
└─$ mkdir Sleuthkit    
                                                                                                     
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos]
└─$ cd Sleuthkit   
                                                                                                     
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/Sleuthkit]
└─$ wget https://artifacts.picoctf.net/c/114/disk.img.gz                                  
--2022-10-13 10:27:56--  https://artifacts.picoctf.net/c/114/disk.img.gz
Resolving artifacts.picoctf.net (artifacts.picoctf.net)... 18.172.134.74, 18.172.134.7, 18.172.134.120, ...
Connecting to artifacts.picoctf.net (artifacts.picoctf.net)|18.172.134.74|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 29714372 (28M) [application/octet-stream]
Saving to: ‘disk.img.gz’

disk.img.gz               100%[==================================>]  28.34M  6.36MB/s    in 5.4s    

2022-10-13 10:28:01 (5.27 MB/s) - ‘disk.img.gz’ saved [29714372/29714372]

                                                                                                     
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/Sleuthkit]
└─$ gzip -d disk.img.gz 
                                                                                                     
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/Sleuthkit]
└─$ file disk.img     
disk.img: DOS/MBR boot sector; partition 1 : ID=0x83, active, start-CHS (0x0,32,33), end-CHS (0xc,190,50), startsector 2048, 202752 sectors
                                                                                                     
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/Sleuthkit]
└─$ mmls -v        
Missing image name
mmls [-i imgtype] [-b dev_sector_size] [-o imgoffset] [-BrvV] [-aAmM] [-t vstype] image [images]
        -t vstype: The type of volume system (use '-t list' for list of supported types)
        -i imgtype: The format of the image file (use '-i list' for list supported types)
        -b dev_sector_size: The size (in bytes) of the device sectors
        -o imgoffset: Offset to the start of the volume that contains the partition system (in sectors)
        -B: print the rounded length in bytes
        -r: recurse and look for other partition tables in partitions (DOS Only)
        -v: verbose output
        -V: print the version
Unless any of these are specified, all volume types are shown
        -a: Show allocated volumes
        -A: Show unallocated volumes
        -m: Show metadata volumes
        -M: Hide metadata volumes
                                                                                                     
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/Sleuthkit]
└─$ mmls disk.img 
DOS Partition Table
Offset Sector: 0
Units are in 512-byte sectors

      Slot      Start        End          Length       Description
000:  Meta      0000000000   0000000000   0000000001   Primary Table (#0)
001:  -------   0000000000   0000002047   0000002048   Unallocated
002:  000:000   0000002048   0000204799   0000202752   Linux (0x83)
                                                                                                     
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/Sleuthkit]
└─$ nc saturn.picoctf.net 52279
What is the size of the Linux partition in the given disk image?
Length in sectors: 0000202752
0000202752
Great work!
picoCTF{mm15_f7w!}
                                                                                                     
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/Sleuthkit]
└─$ 

```
## Notas

## Referencias