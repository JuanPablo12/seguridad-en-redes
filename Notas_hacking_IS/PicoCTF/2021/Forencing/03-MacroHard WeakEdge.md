# MacroHard WeakEdge

## Objetivo
I've hidden a flag in this file. Can you find it? [Forensics is fun.pptm](https://mercury.picoctf.net/static/c00c449c3b08daaccacca6f9d5c55d49/Forensics is fun.pptm)

## Solucion
```bash
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/Macro]
└─$ grep -R pico                                          
                                                                                                    
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/Macro]
└─$ cd ppt  
                                                                                                    
┌──(kali㉿kali)-[~/…/PicoCTF/archivos/Macro/ppt]
└─$ cd slideMasters 
                                                                                                    
┌──(kali㉿kali)-[~/…/archivos/Macro/ppt/slideMasters]
└─$ cat hidden|tr -d ' '|base64 -d
flag: picoCTF{D1d_u_kn0w_ppts_r_z1p5}base64: invalid input
                                                                                                    
┌──(kali㉿kali)-[~/…/archivos/Macro/ppt/slideMasters]
└─$ 

```
## Notas

## Rerencias