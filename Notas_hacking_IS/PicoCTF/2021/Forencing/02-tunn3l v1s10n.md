# tunn3l v1s10n

## Objetivo
We found this [file](https://mercury.picoctf.net/static/21c07c9dd20cd9f2459a0ae75d99af6e/tunn3l_v1s10n). Recover the flag.

## Solucion
```bash
                                                                                                    
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/tunnelVision]
└─$ hexeditor tunn3l_v1s10n.bmp 
                                                                                                    
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/tunnelVision]
└─$ open tunn3l_v1s10n.bmp 
                                                                                                    
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/tunnelVision]
└─$ hexeditor tunn3l_v1s10n.bmp
                                                                                                    
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/tunnelVision]
└─$ exiftool tunn3l_v1s10n.bmp                   
ExifTool Version Number         : 12.44
File Name                       : tunn3l_v1s10n.bmp
Directory                       : .
File Size                       : 2.9 MB
File Modification Date/Time     : 2022:10:11 09:35:00-04:00
File Access Date/Time           : 2022:10:11 09:35:44-04:00
File Inode Change Date/Time     : 2022:10:11 09:35:00-04:00
File Permissions                : -rwxrwx---
File Type                       : BMP
File Type Extension             : bmp
MIME Type                       : image/bmp
BMP Version                     : Windows V3
Image Width                     : 1134
Image Height                    : 306
Planes                          : 1
Bit Depth                       : 24
Compression                     : None
Image Length                    : 2893400
Pixels Per Meter X              : 5669
Pixels Per Meter Y              : 5669
Num Colors                      : Use BitDepth
Num Important Colors            : All
Image Size                      : 1134x306
Megapixels                      : 0.347
                                                                                                    
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/tunnelVision]
└─$ hexeditor tunn3l_v1s10n.bmp
                                                                                                    
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/tunnelVision]
└─$ exiftool tunn3l_v1s10n.bmp 
ExifTool Version Number         : 12.44
File Name                       : tunn3l_v1s10n.bmp
Directory                       : .
File Size                       : 2.9 MB
File Modification Date/Time     : 2022:10:11 09:40:39-04:00
File Access Date/Time           : 2022:10:11 09:40:39-04:00
File Inode Change Date/Time     : 2022:10:11 09:40:39-04:00
File Permissions                : -rwxrwx---
File Type                       : BMP
File Type Extension             : bmp
MIME Type                       : image/bmp
BMP Version                     : Windows V3
Image Width                     : 1134
Image Height                    : 306
Planes                          : 1
Bit Depth                       : 24
Compression                     : None
Image Length                    : 2893400
Pixels Per Meter X              : 5669
Pixels Per Meter Y              : 5669
Num Colors                      : Use BitDepth
Num Important Colors            : All
Image Size                      : 1134x306
Megapixels                      : 0.347
                                                                                                    
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/tunnelVision]
└─$ hexeditor tunn3l_v1s10n.bmp
                                                                                                    
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/tunnelVision]
└─$ open tunn3l_v1s10n.bmp 
                                                                                                    
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/tunnelVision]
└─$ hexeditor tunn3l_v1s10n.bmp
                                                                                                    
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/tunnelVision]
└─$ open tunn3l_v1s10n.bmp     
                                                                                                    
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/tunnelVision]
└─$ 

```
![[tunn3l_v1s10n.bmp]]

## Notas

## Referencias