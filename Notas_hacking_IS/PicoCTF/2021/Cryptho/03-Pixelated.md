# Pixelated


## Objetivo
I have these 2 images, can you make a flag out of them? [scrambled1.png](https://mercury.picoctf.net/static/75e646e4ad19967ca1811f895fb40465/scrambled1.png) [scrambled2.png](https://mercury.picoctf.net/static/75e646e4ad19967ca1811f895fb40465/scrambled2.png)
## Solucion
Ejecutamos java -jar /opt/stegsolve/bin/stegsolve.jar 
Despues se nos despliega una ventana, abrimos la primer imagen, le damos en analyse, combinamos con la 2da imageny le vamos dando en siguiente asta tener el resultado esperado.

picoCTF{d562333d}
![[solved.png]]

O con el siguiente scrip de python:
```python
from PIL import Image
import numpy as np
imagen1 = np.asarray(Image.open('scrambled1.png'))
imagen2 = np.asarray(Image.open('scrambled2.png'))

data = imagen1.copy() + imagen2.copy()

nueva = Image.fromarray(data)
nueva.save("out.png","PNG")
```
## Notas


## Referencias
https://en.wikipedia.org/wiki/Visual_cryptography
https://github.com/zardus/ctf-tools/blob/master/stegsolve/install