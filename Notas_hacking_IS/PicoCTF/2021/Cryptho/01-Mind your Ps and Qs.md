# Mind your Ps and Qs

## Objetivo
In RSA, a small `e` value can be problematic, but what about `N`? Can you decrypt this? [values](https://mercury.picoctf.net/static/bf5e2c8811afb4669f4a6850e097e8aa/values)

## Solucion
```bash
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/MindPQ]
└─$ cat values    
Decrypt my super sick RSA:
c: 421345306292040663864066688931456845278496274597031632020995583473619804626233684
n: 631371953793368771804570727896887140714495090919073481680274581226742748040342637
e: 65537                                                                                                              
┌──(kali㉿kali)-[~/Notas_hacking_IS/PicoCTF/archivos/MindPQ]
└─$ 

```

```python
Type "help", "copyright", "credits" or "license" for more information.
>>> p = 1461849912200000206276283741896701133693
>>> q = 431899300006243611356963607089521499045809
>>> c = 421345306292040663864066688931456845278496274597031632020995583473619804626233684
>>> e = 65537
>>> n = p * q
>>> n
631371953793368771804570727896887140714495090919073481680274581226742748040342637
>>> tb = (p-1)*(q-1)
>>> tn = tb
>>> from Crypto.Util.number import inverse>>> from Crypto.Util.number import long_to_bytes
>>> d = inverse(e,tn)
>>> m = pow(c,d,n)
>>> long_to_bytes(m)
b'picoCTF{sma11_N_n0_g0od_55304594}'
>>> 

```
## Notas
Usamos la pagina que esta en referencias para sacar los valores de p y q, depues solo seguimos la formula para sacar la bandera.

## Referencias
http://factordb.com/