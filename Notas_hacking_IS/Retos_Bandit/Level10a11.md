# Bandit 10 → Level 11

## Objetivo
The password for the next level is stored in the file **data.txt**, which contains base64 encoded data.

## Datos de acceso
usuario: **bandit10**
password: **truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk**

## Solución
**Solucion 1:**
```bash
bandit10@bandit:~$ cat data.txt
VGhlIHBhc3N3b3JkIGlzIElGdWt3S0dzRlc4TU9xM0lSRnFyeEUxaHhUTkViVVBSCg==
bandit10@bandit:~$ cat data.txt | base64 -d
The password is IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR
bandit10@bandit:~$
```

**Solución 2:**
```bash
bandit10@bandit:~$ python
Python 2.7.13 (default, Sep 26 2018, 18:42:22)
[GCC 6.3.0 20170516] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import codecs
>>> data = open('data.txt','r').read()
>>> codecs.decode(data, 'base64')
'The password is IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR\n'
```

## Notas adicionales
La base64 es un tipo de encriptacion, donde para ver que es lo que se quizo decir se utiliza el comando base64 -d
Una segunda forma de solucionarlo es con librerias de python.
O bien se puede usar cyberchef.
## Referencias