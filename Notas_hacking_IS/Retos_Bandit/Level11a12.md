# Bandit 11 → Level 12

## Objetivo
The password for the next level is stored in the file **data.txt**, where all lowercase (a-z) and uppercase (A-Z) letters have been rotated by 13 positions.

## Datos de acceso
Usuario: **bandit11**
Password: **IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR**

## Solución
**Solución 1:**
```bash
bandit11@bandit:~$ cat data.txt
Gur cnffjbeq vf 5Gr8L4qetPEsPk8htqjhRK8XSP6x2RHh
bandit11@bandit:~$ cat data.txt | tr [a-zA-Z] [n-za-mN-ZA-M]
The password is 5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu
bandit11@bandit:~$
```

**Solucion 2:**
```bash
bandit11@bandit:~$ python
Python 2.7.13 (default, Sep 26 2018, 18:42:22)
[GCC 6.3.0 20170516] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import codecs
>>> data = open('data.txt', 'r').read()
>>>
>>> codecs.decode(data, 'rot13')
u'The password is 5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu\n'
```

## Notas adicionales
El comando tr funciona dandole un patron a seguir para que encuentre algo codificado.
## Referencias
