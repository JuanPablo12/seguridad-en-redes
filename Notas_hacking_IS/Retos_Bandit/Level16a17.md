# Bandit 16 → Level 17

## Objetivo
The credentials for the next level can be retrieved by submitting the password of the current level to **a port on localhost in the range 31000 to 32000**. First find out which of these ports have a server listening on them. Then find out which of those speak SSL and which don’t. There is only 1 server that will give the next credentials, the others will simply send back to you whatever you send to it.

## Datos de acceso
Usuario: **bandit16**
Password: **cluFn7wTiGryunymYOu4RcffSxQluehd**

## Solución
```bash

bandit16@bandit:~$ nmap localhost -p 31000-32000

Starting Nmap 7.40 ( https://nmap.org ) at 2022-08-30 16:20 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00030s latency).
Not shown: 996 closed ports
PORT      STATE    SERVICE
31046/tcp open     unknown
31518/tcp filtered unknown
31691/tcp open     unknown
31790/tcp open     unknown
31960/tcp open     unknown

Nmap done: 1 IP address (1 host up) scanned in 1.28 seconds
bandit16@bandit:~$ nc 31518
no port[s] to connect to
bandit16@bandit:~$ opessl s_client -connect localhost 31046
-bash: opessl: command not found
bandit16@bandit:~$ openssl s_client -connect localhost 31046
s_client: Use -help for summary.
bandit16@bandit:~$ openssl s_client -connect localhost 31790
s_client: Use -help for summary.
bandit16@bandit:~$ openssl s_client -connect localhost:31790
CONNECTED(00000003)
depth=0 CN = localhost
verify error:num=18:self signed certificate
verify return:1
depth=0 CN = localhost
verify return:1
---
Certificate chain
 0 s:/CN=localhost
   i:/CN=localhost
---
Server certificate
-----BEGIN CERTIFICATE-----
MIICBjCCAW+gAwIBAgIEJ4dj3zANBgkqhkiG9w0BAQUFADAUMRIwEAYDVQQDDAls
b2NhbGhvc3QwHhcNMjIwODA0MTQ0NzAyWhcNMjMwODA0MTQ0NzAyWjAUMRIwEAYD
VQQDDAlsb2NhbGhvc3QwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAOKNs5Hv
vMkLDL7DAWOObCsOxRATamcwQRU3vKGmhqx3h85DORvg/dcfD+4zMKe4svngMHb/
AvFb7kgx0UmlQ5ZNQ8R13D02iM2KtDzTYhRu72PG6go8xTs4uBVlsMDuzbJJsvu1
ihOxS2/U8k2VgSovPoWdUffAbYVJ04U/d4CfAgMBAAGjZTBjMBQGA1UdEQQNMAuC
CWxvY2FsaG9zdDBLBglghkgBhvhCAQ0EPhY8QXV0b21hdGljYWxseSBnZW5lcmF0
ZWQgYnkgTmNhdC4gU2VlIGh0dHBzOi8vbm1hcC5vcmcvbmNhdC8uMA0GCSqGSIb3
DQEBBQUAA4GBAN/dd5II2+QlVBcfezT+iAs8X35P8LIIiJsSksUJZlgG50V/3xqN
HitXAq4Qxh1KvmcO/09BF/UPHIX9I/aO4/SSJeYKBrtsYh79lySoS2xGzLrYD9vN
vd/FsuBHJ+eE0pM4LMoWEuFOpXavad5KcKk+T4HaXU0A/kL/Upj8jlRR
-----END CERTIFICATE-----
subject=/CN=localhost
issuer=/CN=localhost
---
No client certificate CA names sent
Peer signing digest: SHA512
Server Temp Key: X25519, 253 bits
---
SSL handshake has read 1019 bytes and written 269 bytes
Verification error: self signed certificate
---
New, TLSv1.2, Cipher is ECDHE-RSA-AES256-GCM-SHA384
Server public key is 1024 bit
Secure Renegotiation IS supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : ECDHE-RSA-AES256-GCM-SHA384
    Session-ID: D40B5360F80C2717225E2CB1856074B9B86986B924F148D3CDFCBBBD403A7C2B
    Session-ID-ctx:
    Master-Key: F82F6688FFEEAE8A2EB9BC59DECBCEB2696F64CDD9FCC5D6AF6DBEBB49174C6852C50292E858B9754106B7F94A07650E
    PSK identity: None
    PSK identity hint: None
    SRP username: None
    TLS session ticket lifetime hint: 7200 (seconds)
    TLS session ticket:
    0000 - 88 8b 84 42 b6 05 26 26-21 2f 2b 89 62 bd af 93   ...B..&&!/+.b...
    0010 - 7d cb 2d e9 3c 67 a9 a1-1f d2 ce 23 cb cb 50 68   }.-.<g.....#..Ph
    0020 - 36 75 7a 36 f5 3b 87 d2-a2 cb 8e 9f 68 da eb 55   6uz6.;......h..U
    0030 - c2 35 cf 90 87 de cb 0d-01 6d 13 c1 84 2e 64 5d   .5.......m....d]
    0040 - e7 8c 51 9f 94 cf 48 35-c4 a5 76 c7 55 2e c1 df   ..Q...H5..v.U...
    0050 - fb ee 75 ce ef 54 e1 8d-a1 79 fa 65 27 fe 00 79   ..u..T...y.e''..y
    0060 - 6b 97 04 0b 78 a6 0a cb-e0 7d a4 5f 54 00 d5 15   k...x....}._T...
    0070 - 9f 87 bc 3c 5e a3 25 f6-12 e7 26 8d 67 3d f7 fb   ...<^.%...&.g=..
    0080 - 55 f0 f7 0a 48 af 63 86-c2 3a 0c f1 88 aa 69 7f   U...H.c..:....i.
    0090 - f1 f1 7d 26 1f 47 ce 05-72 12 79 42 58 76 08 a7   ..}&.G..r.yBXv..

    Start Time: 1661869437
    Timeout   : 7200 (sec)
    Verify return code: 18 (self signed certificate)
    Extended master secret: yes
---
BfMYroe26WYalil77FoDi9qh59eK5xNr
Wrong! Please enter the correct current password
closed
bandit16@bandit:~$ openssl s_client -connect localhost:31790
CONNECTED(00000003)
depth=0 CN = localhost
verify error:num=18:self signed certificate
verify return:1
depth=0 CN = localhost
verify return:1
---
Certificate chain
 0 s:/CN=localhost
   i:/CN=localhost
---
Server certificate
-----BEGIN CERTIFICATE-----
MIICBjCCAW+gAwIBAgIEJ4dj3zANBgkqhkiG9w0BAQUFADAUMRIwEAYDVQQDDAls
b2NhbGhvc3QwHhcNMjIwODA0MTQ0NzAyWhcNMjMwODA0MTQ0NzAyWjAUMRIwEAYD
VQQDDAlsb2NhbGhvc3QwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAOKNs5Hv
vMkLDL7DAWOObCsOxRATamcwQRU3vKGmhqx3h85DORvg/dcfD+4zMKe4svngMHb/
AvFb7kgx0UmlQ5ZNQ8R13D02iM2KtDzTYhRu72PG6go8xTs4uBVlsMDuzbJJsvu1
ihOxS2/U8k2VgSovPoWdUffAbYVJ04U/d4CfAgMBAAGjZTBjMBQGA1UdEQQNMAuC
CWxvY2FsaG9zdDBLBglghkgBhvhCAQ0EPhY8QXV0b21hdGljYWxseSBnZW5lcmF0
ZWQgYnkgTmNhdC4gU2VlIGh0dHBzOi8vbm1hcC5vcmcvbmNhdC8uMA0GCSqGSIb3
DQEBBQUAA4GBAN/dd5II2+QlVBcfezT+iAs8X35P8LIIiJsSksUJZlgG50V/3xqN
HitXAq4Qxh1KvmcO/09BF/UPHIX9I/aO4/SSJeYKBrtsYh79lySoS2xGzLrYD9vN
vd/FsuBHJ+eE0pM4LMoWEuFOpXavad5KcKk+T4HaXU0A/kL/Upj8jlRR
-----END CERTIFICATE-----
subject=/CN=localhost
issuer=/CN=localhost
---
No client certificate CA names sent
Peer signing digest: SHA512
Server Temp Key: X25519, 253 bits
---
SSL handshake has read 1019 bytes and written 269 bytes
Verification error: self signed certificate
---
New, TLSv1.2, Cipher is ECDHE-RSA-AES256-GCM-SHA384
Server public key is 1024 bit
Secure Renegotiation IS supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : ECDHE-RSA-AES256-GCM-SHA384
    Session-ID: 16D460ABE85472DA1A8A63F57599257D837902DAF82FCB49BE779893971BDDF0
    Session-ID-ctx:
    Master-Key: DF5D1685708FB7E44EEF9FF9286A013894B96CE90F68CCD5ADF7D59CAB808534FCED1EA329A0B99164058702C3A53C74
    PSK identity: None
    PSK identity hint: None
    SRP username: None
    TLS session ticket lifetime hint: 7200 (seconds)
    TLS session ticket:
    0000 - 88 8b 84 42 b6 05 26 26-21 2f 2b 89 62 bd af 93   ...B..&&!/+.b...
    0010 - d7 c3 62 c8 27 07 ba 45-20 a9 42 64 0e 5f 2c 0b   ..b.''..E .Bd._,.
    0020 - dc 61 af 45 99 69 81 1f-55 e0 c0 81 38 6c ab 48   .a.E.i..U...8l.H
    0030 - 53 16 9e 83 fe 80 75 82-8b 28 a7 0b 8c c6 9f e0   S.....u..(......
    0040 - a0 51 c9 90 cb 27 04 f3-e3 82 2e 56 ad b4 db 15   .Q...'.....V....
    0050 - 97 80 32 6a 04 69 3e cc-42 37 27 3e a3 ca 3e f2   ..2j.i>.B7'>..>.
    0060 - 5d fb d2 a4 f9 87 92 6b-ae a4 14 46 9e f1 87 b5   ]......k...F....
    0070 - 86 e7 10 14 84 80 d3 d5-1d a2 3d d5 e7 82 b8 a8   ..........=.....
    0080 - c5 e9 65 f4 a7 86 66 e2-c2 be 21 7e e1 0c fd 42   ..e...f...!~...B
    0090 - 38 ac dc d4 3a 6e 4a 68-1b ec 06 79 97 b3 86 15   8...:nJh...y....

    Start Time: 1661869457
    Timeout   : 7200 (sec)
    Verify return code: 18 (self signed certificate)
    Extended master secret: yes
---
cluFn7wTiGryunymYOu4RcffSxQluehd
Correct!
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAvmOkuifmMg6HL2YPIOjon6iWfbp7c3jx34YkYWqUH57SUdyJ
imZzeyGC0gtZPGujUSxiJSWI/oTqexh+cAMTSMlOJf7+BrJObArnxd9Y7YT2bRPQ
Ja6Lzb558YW3FZl87ORiO+rW4LCDCNd2lUvLE/GL2GWyuKN0K5iCd5TbtJzEkQTu
DSt2mcNn4rhAL+JFr56o4T6z8WWAW18BR6yGrMq7Q/kALHYW3OekePQAzL0VUYbW
JGTi65CxbCnzc/w4+mqQyvmzpWtMAzJTzAzQxNbkR2MBGySxDLrjg0LWN6sK7wNX
x0YVztz/zbIkPjfkU1jHS+9EbVNj+D1XFOJuaQIDAQABAoIBABagpxpM1aoLWfvD
KHcj10nqcoBc4oE11aFYQwik7xfW+24pRNuDE6SFthOar69jp5RlLwD1NhPx3iBl
J9nOM8OJ0VToum43UOS8YxF8WwhXriYGnc1sskbwpXOUDc9uX4+UESzH22P29ovd
d8WErY0gPxun8pbJLmxkAtWNhpMvfe0050vk9TL5wqbu9AlbssgTcCXkMQnPw9nC
YNN6DDP2lbcBrvgT9YCNL6C+ZKufD52yOQ9qOkwFTEQpjtF4uNtJom+asvlpmS8A
vLY9r60wYSvmZhNqBUrj7lyCtXMIu1kkd4w7F77k+DjHoAXyxcUp1DGL51sOmama
+TOWWgECgYEA8JtPxP0GRJ+IQkX262jM3dEIkza8ky5moIwUqYdsx0NxHgRRhORT
8c8hAuRBb2G82so8vUHk/fur85OEfc9TncnCY2crpoqsghifKLxrLgtT+qDpfZnx
SatLdt8GfQ85yA7hnWWJ2MxF3NaeSDm75Lsm+tBbAiyc9P2jGRNtMSkCgYEAypHd
HCctNi/FwjulhttFx/rHYKhLidZDFYeiE/v45bN4yFm8x7R/b0iE7KaszX+Exdvt
SghaTdcG0Knyw1bpJVyusavPzpaJMjdJ6tcFhVAbAjm7enCIvGCSx+X3l5SiWg0A
R57hJglezIiVjv3aGwHwvlZvtszK6zV6oXFAu0ECgYAbjo46T4hyP5tJi93V5HDi
Ttiek7xRVxUl+iU7rWkGAXFpMLFteQEsRr7PJ/lemmEY5eTDAFMLy9FL2m9oQWCg
R8VdwSk8r9FGLS+9aKcV5PI/WEKlwgXinB3OhYimtiG2Cg5JCqIZFHxD6MjEGOiu
L8ktHMPvodBwNsSBULpG0QKBgBAplTfC1HOnWiMGOU3KPwYWt0O6CdTkmJOmL8Ni
blh9elyZ9FsGxsgtRBXRsqXuz7wtsQAgLHxbdLq/ZJQ7YfzOKU4ZxEnabvXnvWkU
YOdjHdSOoKvDQNWu6ucyLRAWFuISeXw9a/9p7ftpxm0TSgyvmfLF2MIAEwyzRqaM
77pBAoGAMmjmIJdjp+Ez8duyn3ieo36yrttF5NSsJLAbxFpdlc1gvtGCWW+9Cq0b
dxviW8+TFVEBl1O4f7HVm6EpTscdDxU+bCXWkfjuRb7Dy9GOtt9JPsX8MBTakzh3
vBgsyi/sN3RqRBcGU40fOoZyfAMT8s1m/uYv52O6IgeuZ/ujbjY=
-----END RSA PRIVATE KEY-----

closed
```

## Notas adicionales
El comando nmap nos ayuda a escanear los puertos que estan abiertos, aunque este solo escanea algunos puertos podemos indicarle un rango para ser más específicos y ver si el purto que buscamos está o no abierto.
Guardando la llave en un archivo podemos ingresarla con el comando ssh -i nombreDelArchivo 
para poder ingresar sin la contraseña.
## Referencias

