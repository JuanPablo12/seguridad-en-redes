# Bandit 9 → Level 10

## Objetivo
The password for the next level is stored in the file **data.txt** in one of the few human-readable strings, preceded by several ‘=’ characters.

## Datos de acceso
username: **bandit9**
password: **UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR**

## Solución


```bash
bandit9@bandit:~$ file data.txt
data.txt: data
bandit9@bandit:~$ ls -la
total 40
drwxr-xr-x  2 root     root     4096 May  7  2020 .
drwxr-xr-x 41 root     root     4096 May  7  2020 ..
-rw-r--r--  1 root     root      220 May 15  2017 .bash_logout
-rw-r--r--  1 root     root     3526 May 15  2017 .bashrc
-rw-r-----  1 bandit10 bandit9 19379 May  7  2020 data.txt
-rw-r--r--  1 root     root      675 May 15  2017 .profile
bandit9@bandit:~$ strings -n 30 data.txt | grep ==
&========== truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk
bandit9@bandit:~$
```

## Notas adicionales
strings: permite buscar dentro de un archivo. Despliega las cadenas imprimibles dentro de un archivo.
strings -n 30 data.txt | grep = =, en este comando con el -n hacemos que nos muestre las oraciones de mas de 30 caracteres y el grep filtra los que empiezen con el signo de ==
## Referencias
https://respontodo.com/como-usar-el-comando-strings-en-linux/