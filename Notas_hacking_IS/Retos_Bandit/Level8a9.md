# Bandit 8 → Level 9

## Objetivo
The password for the next level is stored in the file **data.txt** and is the only line of text that occurs only once.

## Datos de acceso
username: **bandit8**
password: **cvX2JJa4CFALtqS87jk27qwqGhBM9plV**

## Solución

```bash
bandit8@bandit:~$ ls
data.txt
bandit8@bandit:~$ cat data.txt | sort | uniq -u
UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR
bandit8@bandit:~$
```

## Notas adicionales
sort: ordena lineas de texto.
uniq: nos permite contar las lineas que se repite algo en un archivo con -c, mostrar los que se repiten solo una vez con -u. 
Algunos comando que nos daran la respuesta pueden ser:
cat data.txt | sort | uniq -c
cat data.txt | sort | uniq -u

## Referencias
https://forum.obsidian.md/t/sort-text-selection-alphabetically/10975
https://ryanstutorials.net/linuxtutorial/piping.php
