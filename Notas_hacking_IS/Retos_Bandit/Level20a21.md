# Bandit 20 → Level 21

## Objetivo
There is a setuid binary in the homedirectory that does the following: it makes a connection to localhost on the port you specify as a commandline argument. It then reads a line of text from the connection and compares it to the password in the previous level (bandit20). If the password is correct, it will transmit the password for the next level (bandit21).

**NOTE:** Try connecting to your own network daemon to see if it works as you think

## Datos de acceso
Usuario: **bandit20**
Password **VxCazJaVykI6W36BkBU0mJTCM8rR95XT**

## Solución
```bash
bandit20@bandit:~$ nc -lvp 3035 <<< VxCazJaVykI6W36BkBU0mJTCM8rR95XT &
[1] 635329
bandit20@bandit:~$ Listening on 0.0.0.0 3035

bandit20@bandit:~$ ./suconnect  3035
Connection received on localhost 54138
Read: VxCazJaVykI6W36BkBU0mJTCM8rR95XT
Password matches, sending next password
NvEJF7oVjkddltPSrdKEFOllh9V1IBcq
[1]+  Done                    nc -lvp 3035 <<< VxCazJaVykI6W36BkBU0mJTCM8rR95XT
bandit20@bandit:~$
```

## Notas adicionales
Abrimo un puerto en nuestra computadora y al dejarlo en segundo plano enviamos el password del nivel para que nos de el siguiente.

## Referencias
