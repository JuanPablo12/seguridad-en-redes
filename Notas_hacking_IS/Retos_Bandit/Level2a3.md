# Bandit Level 2 → Level 3

## Objetivo
The password for the next level is stored in a file called **spaces in this filename** located in the home directory

## Datos de acceso
username: **bandit2**
password: **CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9**

## Solución

```bash
bandit2@bandit:~$ ls
spaces in this filename
bandit2@bandit:~$ cat "spaces in this filename"
UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK
bandit2@bandit:~$
```

## Notas adicionales
Cuando un archivo tiene espacios en el nombre se pueden usar comillas para delimitarla o un guion en los lugares donde hay espacios.
## Referencias
https://docs.microsoft.com/en-us/troubleshoot/windows-server/deployment/filenames-with-spaces-require-quotation-mark#:~:text=Spaces%20are%20allowed%20in%20long,word%20to%20specify%20a%20parameter.
