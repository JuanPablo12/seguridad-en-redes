# Bandit Level 3 → Level 4

## Objetivo
The password for the next level is stored in a hidden file in the **inhere** directory.

## Datos de acceso
username: **bandit3**
password: **UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK**

## Solución
```bash
bandit3@bandit:~$ ls
inhere
bandit3@bandit:~$ cd inhere/
bandit3@bandit:~/inhere$ ls
bandit3@bandit:~/inhere$ ls -a
.  ..  .hidden
bandit3@bandit:~/inhere$ cat .hidden
pIwrPrtPN36QITSp3EQaw936yaFoFgAB
bandit3@bandit:~/inhere$
```

## Notas adicionales

## Referencias
https://man7.org/linux/man-pages/man1/cd.1p.html
