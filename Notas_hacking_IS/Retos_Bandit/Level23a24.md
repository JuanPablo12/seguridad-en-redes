# Bandit 23 → Level 24

## Objetivo
A program is running automatically at regular intervals from **cron**, the time-based job scheduler. Look in **/etc/cron.d/** for the configuration and see what command is being executed.

**NOTE:** This level requires you to create your own first shell-script. This is a very big step and you should be proud of yourself when you beat this level!

**NOTE 2:** Keep in mind that your shell script is removed once executed, so you may want to keep a copy around…

## Datos de acceso
Usuario: **bandit23**
Password **QYw0Y2aiA672PsMmh9puTQuhoz8SyR2G**

## Solución
```bash
bandit23@bandit:~$ cat /etc/cron.d/cronjob_bandit24
@reboot bandit24 /usr/bin/cronjob_bandit24.sh &> /dev/null
* * * * * bandit24 /usr/bin/cronjob_bandit24.sh &> /dev/null
bandit23@bandit:~$ cat /usr/bin/cronjob_bandit24.sh
#!/bin/bash

myname=$(whoami)

cd /var/spool/$myname/foo
echo "Executing and deleting all scripts in /var/spool/$myname/foo:"
for i in * .*;
do
    if [ "$i" != "." -a "$i" != ".." ];
    then
        echo "Handling $i"
        owner="$(stat --format "%U" ./$i)"
        if [ "${owner}" = "bandit23" ]; then
            timeout -s 9 60 ./$i
        fi
        rm -f ./$i
    fi
done

bandit23@bandit:~$ mkdir /tem/juantem
mkdir: cannot create directory ‘/tem/juantem’: No such file or directory
bandit23@bandit:~$ mkdir /tmp/juantem
bandit23@bandit:~$ cd /tmp/juantem
bandit23@bandit:/tmp/juantem$ echo "cat /etc/bandit_pass/bandit24 > /tmp/juantem/password" > juantem.sh
bandit23@bandit:/tmp/juantem$ chmod 777 juantem.sh
bandit23@bandit:/tmp/juantem$ touch password
bandit23@bandit:/tmp/juantem$ chmod 666 password
bandit23@bandit:/tmp/juantem$ ls -la
total 192
drwxrwxr-x    2 bandit23 bandit23   4096 Sep  6 13:32 .
drwxrwx-wt 4821 root     root     184320 Sep  6 13:32 ..
-rwxrwxrwx    1 bandit23 bandit23     54 Sep  6 13:31 juantem.sh
-rw-rw-rw-    1 bandit23 bandit23      0 Sep  6 13:32 password
bandit23@bandit:/tmp/juantem$ cp juantem.sh /var/spool/bandit24/foo/
bandit23@bandit:/tmp/juantem$ date
Tue Sep  6 01:34:42 PM UTC 2022
bandit23@bandit:/tmp/juantem$ cp juantem.sh /var/spool/bandit24/foo
bandit23@bandit:/tmp/juantem$ date
Tue Sep  6 01:35:16 PM UTC 2022
bandit23@bandit:/tmp/juantem$ cat password
VAfGXJ1PBSsPSnvsjI8p759leLZ9GGar
bandit23@bandit:/tmp/juantem
```

## Notas adicionales

## Referencias
