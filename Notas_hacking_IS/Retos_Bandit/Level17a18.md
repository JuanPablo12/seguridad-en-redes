# Bandit 17 → Level 18

## Objetivo
There are 2 files in the homedirectory: **passwords.old and passwords.new**. The password for the next level is in **passwords.new** and is the only line that has been changed between **passwords.old and passwords.new**

## Datos de acceso
Usuario: **bandit17**
Password: **VwOSWtCA7lRKkTfbr2IDh6awj9RNZM5e**

## Solución
```bash

bandit17@bandit:~$ diff passwords.old passwords.new
42c42
< 09wUIyMU4YhOzl1Lzxoz0voIBzZ2TUAf
---
> hga5tuuCLF6fFzUpnagiMN8ssu9LFrdg
bandit17@bandit:~$
```

## Notas adicionales
El comando diff nos ayuda a saber las duferencias que hay entre archivos, en este caso nos dice que la linea 42 es la que esta diferente.
El comando head muestra las primeras 10 lineas y al agregar -n podemos definir el número de líneas.
El comando tail nos muestra las últimas 10 líneas, también se le puede agregar el comando -n.
## Referencias
