# Bandit Level 1 → Level 2

## Objetivo
The password for the next level is stored in a file called **-** located in the home directory

## Datos de acceso
username: **bandit1**
password: **boJ9jbbUNNfktd78OOpsqOltutMc3MY1**
## Solución
```bash
bandit1@bandit:~$ ls
-
bandit1@bandit:~$ cat ./-
CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9
bandit1@bandit:~$
```

## Notas adicionales
El comando cat confunde al - como parte la entrada estandar, por lo tanto si lo ponemos directamente asi no nos lo reconocera.

## Referencias
https://man7.org/linux/man-pages/man1/cat.1.html
https://man7.org/linux/man-pages/man1/ls.1.html
https://www.webservertalk.com/dashed-filename#:~:text=Dashed%20Filename%20%E2%80%93%20Learn%20How%20to,%2C%20List%2C%20Read%20%26%20Copy!&text=In%20Unix%20or%20Linux%20operating,to%20specify%20options%20and%20arguments.
