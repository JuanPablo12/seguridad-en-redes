# Bandit 15 → Level 16

## Objetivo
The password for the next level can be retrieved by submitting the password of the current level to **port 30001 on localhost** using SSL encryption.

**Helpful note: Getting “HEARTBEATING” and “Read R BLOCK”? Use -ign_eof and read the “CONNECTED COMMANDS” section in the manpage. Next to ‘R’ and ‘Q’, the ‘B’ command also works in this version of that command…**

## Datos de acceso
Usuario: **bandit15**
Password: **BfMYroe26WYalil77FoDi9qh59eK5xNr**

## Solución
```bash
bandit15@bandit:~$ openssl s_client -connect localhost:30001
CONNECTED(00000003)
depth=0 CN = localhost
verify error:num=18:self signed certificate
verify return:1
depth=0 CN = localhost
verify return:1
---
Certificate chain
 0 s:/CN=localhost
   i:/CN=localhost
---
Server certificate
-----BEGIN CERTIFICATE-----
MIICBjCCAW+gAwIBAgIEXcVbPTANBgkqhkiG9w0BAQUFADAUMRIwEAYDVQQDDAls
b2NhbGhvc3QwHhcNMjIwMzA5MTk0NzQyWhcNMjMwMzA5MTk0NzQyWjAUMRIwEAYD
VQQDDAlsb2NhbGhvc3QwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBALDCas6k
DHxTRoxVISHtXOeCwJ8Sax5BZN76Hle8AH6pYTAdv9/FRssWL1xppFAtiGnFvglu
95FJvHEQirY4F0oPBTbtGU2xhzZzkWRL5Yj2C3Q2c99cyh+uWQT7sXPtB8W1osPc
YIo83YkXiArpt28474ZYdl+ohbPtP1oQHBv3AgMBAAGjZTBjMBQGA1UdEQQNMAuC
CWxvY2FsaG9zdDBLBglghkgBhvhCAQ0EPhY8QXV0b21hdGljYWxseSBnZW5lcmF0
ZWQgYnkgTmNhdC4gU2VlIGh0dHBzOi8vbm1hcC5vcmcvbmNhdC8uMA0GCSqGSIb3
DQEBBQUAA4GBAC2693WiK/kXMCauf1fEg5DwuxIfm0saYKiLSceyZo1G4IggqOBO
9JCtvMIV/xRAmYEnPvJmf0JtYv+2fsicaPh9E1GRmU0vGoYDZzA7NTZOgRmHlRKe
ihh/XSGrY7tE1qU+EfizmhcB35iZ7W5INIKlu7oyBWcvk3rI4jtPQeZp
-----END CERTIFICATE-----
subject=/CN=localhost
issuer=/CN=localhost
---
No client certificate CA names sent
Peer signing digest: SHA512
Server Temp Key: X25519, 253 bits
---
SSL handshake has read 1019 bytes and written 269 bytes
Verification error: self signed certificate
---
New, TLSv1.2, Cipher is ECDHE-RSA-AES256-GCM-SHA384
Server public key is 1024 bit
Secure Renegotiation IS supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : ECDHE-RSA-AES256-GCM-SHA384
    Session-ID: 9A922F3FEC77D6DC1BDADADDF726D682271AFAE2A82469812852DEB5C675CD67
    Session-ID-ctx:
    Master-Key: 37795A83C0A578B7113E622034F512A6CFA64D2F1324693876A67685189733652187BFDCD830D8CEC732CA7E407E65A6
    PSK identity: None
    PSK identity hint: None
    SRP username: None
    TLS session ticket lifetime hint: 7200 (seconds)
    TLS session ticket:
    0000 - d3 53 01 d9 4c 2e 82 50-ef 16 38 c5 d8 1b dd f2   .S..L..P..8.....
    0010 - 79 7c 0c 89 e3 c3 77 33-73 2a dd e6 c9 b7 a6 bf   y|....w3s*......
    0020 - e3 e7 ff 4b 5e 20 bf 14-94 36 59 4c 97 fd 3f f1   ...K^ ...6YL..?.
    0030 - 23 9f b0 f3 ba 88 c0 b5-41 4a a1 68 a9 e1 b9 90   #.......AJ.h....
    0040 - a0 ea 37 1a 53 f5 bd 2b-45 2a ba 1d 10 8b 79 9e   ..7.S..+E*....y.
    0050 - 32 f1 7d 1c b3 79 f9 3b-31 bb d5 40 27 04 e7 b2   2.}..y.;1..@'...
    0060 - 8f cd 4c 77 ae 3e 9f 14-ab f2 11 92 af 6d 5f 24   ..Lw.>.......m_$
    0070 - 46 d2 b5 2b 4a 64 f3 62-bf 8b 4d 07 72 2c 1a 3e   F..+Jd.b..M.r,.>
    0080 - 7e e0 b5 c5 8a 9c cf e8-2d 47 17 a6 80 b4 12 a7   ~.......-G......
    0090 - 05 d1 61 fb f2 7f c5 7c-01 fc af 3c 60 18 57 b9   ..a....|...<`.W.
'
    Start Time: 1661868597
    Timeout   : 7200 (sec)
    Verify return code: 18 (self signed certificate)
    Extended master secret: yes
---
BfMYroe26WYalil77FoDi9qh59eK5xNr
Correct!
cluFn7wTiGryunymYOu4RcffSxQluehd

closed
bandit15@bandit:~$
```

## Notas adicionales

## Referencias


