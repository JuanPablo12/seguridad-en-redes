# Bandit 7 → Level 8

## Objetivo
The password for the next level is stored in the file **data.txt** next to the word **millionth**

## Datos de acceso
username: **bandit7**
password: **HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs**

## Solución

```bash
bandit7@bandit:~$ ls
data.txt
bandit7@bandit:~$ grep millionth data.txt
millionth       cvX2JJa4CFALtqS87jk27qwqGhBM9plV
bandit7@bandit:~$
```

## Notas adicionales
cat data.txt | grep millionth | wc, nos da detalles sobre el archivo.
Grep: es un patron que me permite buscar en un archivo de texto.
El signo | nos sirve para concatenar 2 o mas comandos en una misma linea.
## Referencias
https://www.reddit.com/r/ObsidianMD/comments/p0umt4/plugin_to_grepfilter_open_file/
https://stackoverflow.com/questions/71420810/grep-and-other-programs-not-found-in-script